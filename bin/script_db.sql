use reservas_db;

create table rol(
    rol_id int not null auto_increment,
    descripcion varchar(255),
    primary key(rol_id)
);


create table usuario(
    usuario_id int not null auto_increment,
    nombre varchar(255),
    usuario_nombre varchar(255),
    password varchar(255),
    calificacion int,
    apellido varchar(255),
    email varchar(255),
    rol_id int not null,
    primary key(usuario_id),
    constraint usuario_rol foreign key (rol_id) references rol(rol_id)
);

create table organizacion(
    organizacion_id int not null auto_increment,
    descripcion varchar(255),
    email varchar(255),
    primary key(organizacion_id)
);

create table usuario_organizacion(
    usuario_organizacion_id int not null auto_increment,
    usuario_id int not null,
    organizacion_id int not null,
    primary key(usuario_organizacion_id),
    constraint usuario_organizacion_usuario foreign key (usuario_id) references usuario(usuario_id),
    constraint usuario_organizacion_organizacion foreign key (organizacion_id) references organizacion(organizacion_id)
);

create table estado_reserva(
    estado_reserva_id int not null auto_increment,
    descripcion varchar(255),
    primary key(estado_reserva_id)
);

create table reserva(
    reserva_id int not null auto_increment,
    estado_reserva_id int not null,
    fecha_reserva datetime,
	usuario_id int not null,
    primary key(reserva_id),
	constraint reserva_estado_reserva foreign key (estado_reserva_id) references estado_reserva(estado_reserva_id),
	constraint reserva_usuario foreign key (usuario_id) references usuario(usuario_id)
);


create table producto(
    producto_id int not null auto_increment,
	reserva_id int,
    descripcion varchar(255),
    imagen LONGBLOB,
    primary key(producto_id),
	constraint producto_reserva foreign key (reserva_id) references reserva(reserva_id)
);

create table servicio(
    servicio_id int not null auto_increment,
    reserva_id int,
	descripcion varchar(255),
    imagen LONGBLOB,
    primary key(servicio_id),
	constraint servicio_reserva foreign key (reserva_id) references reserva(reserva_id)
);




