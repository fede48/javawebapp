package com.unaj.javawebapp.repository;

import com.unaj.javawebapp.model.Organizacion;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository("repositoryOrganizacion")
public interface OrganizacionRepository extends JpaRepository<Organizacion,Serializable>{
	
	public abstract Organizacion findById(int id);
	
	public abstract Organizacion findByEmail(String email);
	
}
