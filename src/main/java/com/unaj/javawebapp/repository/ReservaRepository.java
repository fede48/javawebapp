package com.unaj.javawebapp.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.unaj.javawebapp.model.Reserva;

@Repository("repositoryReserva")
public interface ReservaRepository extends JpaRepository<Reserva,Serializable> {
	
	@Query(value="select * from reserva where user_id = (:id)", nativeQuery=true)
	Reserva findByUser(@Param("id") int id);

}
