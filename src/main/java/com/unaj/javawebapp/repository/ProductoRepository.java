package com.unaj.javawebapp.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.unaj.javawebapp.model.Producto;

@Repository("repositoryProducto")
public interface ProductoRepository extends JpaRepository<Producto,Serializable> {
	
	@Query(value="select * from producto where organizacion_id = (:id)", nativeQuery=true)
	List<Producto> findByOrganizacion(@Param("id") int id);
}
