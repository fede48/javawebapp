package com.unaj.javawebapp.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unaj.javawebapp.model.ReservaProducto;

public interface ReservaProductoRepository extends JpaRepository<ReservaProducto,Serializable> {

}
