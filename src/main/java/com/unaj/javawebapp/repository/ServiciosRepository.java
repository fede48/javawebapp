package com.unaj.javawebapp.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.unaj.javawebapp.model.Producto;
import com.unaj.javawebapp.model.Servicios;

@Repository("serviciosRepository")
public interface ServiciosRepository extends JpaRepository<Servicios, Serializable>  {
	
	@Query(value="select * from servicios where organizacion_id = (:id)", nativeQuery=true)
	List<Servicios> findByOrganizacion(@Param("id") int id);

}
