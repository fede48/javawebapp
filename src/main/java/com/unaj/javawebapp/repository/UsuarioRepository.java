package com.unaj.javawebapp.repository;

import com.unaj.javawebapp.model.Usuario;

import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository("repositoryUsuario")
public interface UsuarioRepository extends JpaRepository<Usuario, Serializable>
{
	
	@Query(value="select * from usuario where username = (:username)", nativeQuery=true)
	Usuario findByUserName(@Param("username") String username);

}
