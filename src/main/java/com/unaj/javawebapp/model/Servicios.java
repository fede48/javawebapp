package com.unaj.javawebapp.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity

public class Servicios {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("id")
	private int id;
	
	@Column
	@JsonProperty("descripcion")
	private String descripcion;
	
	@Column(nullable = true)
	@JsonProperty("imagen")
	private Byte imagen;
	
	@Column
	@JsonProperty("codigo")
	private String codigo;
	
	@Column
	@JsonProperty("disponible")
	private boolean disponible = true;


	@OneToMany(targetEntity = ReservaServicio.class)
	@JsonProperty("reservaservicio")
	private List<ReservaServicio>reservaservicio;
	
	@ManyToOne(optional = false , targetEntity = Organizacion.class)
	@JsonProperty("organizacion")
	private Organizacion organizacion;

	public Servicios() {
		
	}



	public Servicios(int id, String descripcion, byte imagen, List<ReservaServicio> reservaservicio,
			Organizacion organizacion,String codigo, boolean disponible) {
		this.id = id;
		this.descripcion = descripcion;
		this.imagen = imagen;
		this.reservaservicio = reservaservicio;
		this.organizacion = organizacion;
		this.codigo=codigo;
		this.disponible = disponible;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public byte getImagen() {
		return imagen;
	}

	public void setImagen(byte imagen) {
		this.imagen = imagen;
	}






	public Organizacion getOrganizacion() {
		return organizacion;
	}



	public void setOrganizacion(Organizacion organizacion) {
		this.organizacion = organizacion;
	}


	public List<ReservaServicio> getReservaservicio() {
		return reservaservicio;
	}



	public void setReservaservicio(List<ReservaServicio> reservaservicio) {
		this.reservaservicio = reservaservicio;
	}



	public String getCodigo() {
		return codigo;
	}



	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	
	public boolean isDisponible() {
		return disponible;
	}



	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}

	
	
	
	
	
	
	
	
	
	

}
