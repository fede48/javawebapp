package com.unaj.javawebapp.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonProperty;

import net.bytebuddy.implementation.bind.annotation.Default;

@Entity
public class Producto {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("id")
	private int id;
	
	@Column
	@JsonProperty("descripcion")
	private String descripcion;

	@Column
	@JsonProperty("imagen")
	private Byte imagen=0;
	
	@Column
	@JsonProperty("codigo")
	private String codigo;

	@Column
	@JsonProperty("disponible")
	private boolean disponible = true;
		
	// el optional es para declarar la variable como NOTNULL.
	@ManyToOne(optional = false , targetEntity = Organizacion.class)
	@JsonProperty("organizacion")
	private Organizacion organizacion;
	
	
	@OneToMany(targetEntity = ReservaProducto.class)
	@JsonProperty("reservaproducto")
	private List<ReservaProducto>reservaproducto;
	
	

	public Producto() {
		
	}


	public Producto(int id, String descripcion, byte imagen, Organizacion organizacion, List<ReservaProducto> reservas, String codigo, boolean disponible) {
		this.id = id;
		this.descripcion = descripcion;
		this.imagen = imagen;
		this.organizacion = organizacion;
		this.reservaproducto = reservas;
		this.codigo=codigo;
		this.disponible = disponible;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}	
	public boolean isDisponible() {
		return disponible;
	}



	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}

	public List<ReservaProducto> getReservaproducto() {
		return reservaproducto;
	}

	public void setReservaproducto(List<ReservaProducto> reservaproducto) {
		this.reservaproducto = reservaproducto;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public Organizacion getOrganizacion() {
		return organizacion;
	}


	public void setOrganizacion(Organizacion organizacion) {
		this.organizacion = organizacion;
	}


	public Byte getImagen() {
		return imagen;
	}


	public void setImagen(Byte imagen) {
		this.imagen = imagen;
	}
	
	
	


	
	
}
