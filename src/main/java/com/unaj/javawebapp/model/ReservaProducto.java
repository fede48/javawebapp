package com.unaj.javawebapp.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class ReservaProducto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("id")
	private int id;
	
	@Column
	@JsonProperty("fechaReservada")
	private Date fechaReservada;
	
	@Column
	@JsonProperty("fechaDevuelta")
	private Date fechaDevuelta;
	
	@ManyToOne(targetEntity = Producto.class, optional = false)
	@JsonProperty("producto")
	private Producto producto;
	
	@ManyToOne(targetEntity = Reserva.class)
	@JsonProperty("reserva")
	private Reserva reserva;
	
	
	public ReservaProducto() {
		
	}

	

	public ReservaProducto(int id, Date fechaReservada, Date fechaDevuelta, Producto producto, Reserva reserva) {
		this.id = id;
		this.fechaReservada = fechaReservada;
		this.fechaDevuelta = fechaDevuelta;
		this.producto = producto;
		this.reserva = reserva;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Reserva getReserva() {
		return reserva;
	}

	public void setReserva(Reserva reserva) {
		this.reserva = reserva;
	}



	public Date getFechaReservada() {
		return fechaReservada;
	}



	public void setFechaReservada(Date fechaReservada) {
		this.fechaReservada = fechaReservada;
	}



	public Date getFechaDevuelta() {
		return fechaDevuelta;
	}



	public void setFechaDevuelta(Date fechaDevuelta) {
		this.fechaDevuelta = fechaDevuelta;
	}
	
	
	
	
	
	
	
	

}
