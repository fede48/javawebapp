package com.unaj.javawebapp.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.*;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Reserva {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("id")
	private int id;
	
	
	@JsonProperty("fechareserva")
	@Column(name="fecha_reserva")
	private Date fechareserva;
	
	
	@OneToOne(targetEntity = EstadoReserva.class)
	@JsonProperty("estadoreserva")
	private EstadoReserva estadoreserva;
	
	@OneToOne(targetEntity = Usuario.class,optional=false)
	@JsonProperty("usuario")
	private Usuario user;
	

	@OneToMany (cascade = CascadeType.ALL, mappedBy = "reserva")
	@JsonIgnoreProperties("reserva")
	private List<ReservaProducto>reservaproducto;
	
	@OneToMany
	@JsonIgnore
	@JsonProperty("reservaservicio")
	private List<ReservaServicio>reservaservicio;


	public Reserva() {
	}

	public Reserva(int id, Date fechareserva, EstadoReserva estadoreserva, Usuario user,
			List<ReservaProducto> reservaproducto, List<ReservaServicio> reservaservicio) {
		this.id = id;
		this.fechareserva = fechareserva;
		this.estadoreserva = estadoreserva;
		this.user = user;
		this.reservaproducto = reservaproducto;
		this.reservaservicio = reservaservicio;
	}

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	public Date getFechareserva() {
		return fechareserva;
	}


	public void setFechareserva(Date fechareserva) {
		this.fechareserva = fechareserva;
	}


	public Usuario getUser() {
		return user;
	}


	public void setUser(Usuario user) {
		this.user = user;
	}


	public EstadoReserva getEstadoreserva() {
		return estadoreserva;
	}


	public void setEstadoreserva(EstadoReserva estadoreserva) {
		this.estadoreserva = estadoreserva;
	}

	public List<ReservaProducto> getReservaproducto() {
		return reservaproducto;
	}

	public void setReservaproducto(List<ReservaProducto> reservaproducto) {
		this.reservaproducto = reservaproducto;
	}

	public List<ReservaServicio> getReservaservicio() {
		return reservaservicio;
	}

	public void setReservaservicio(List<ReservaServicio> reservaservicio) {
		this.reservaservicio = reservaservicio;
	}
	
	
}
