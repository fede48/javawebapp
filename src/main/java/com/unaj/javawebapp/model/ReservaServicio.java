package com.unaj.javawebapp.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class ReservaServicio {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column
	private Date fechaReservada;
	
	@Column
	private Date fechaDevuelta;
	
	@ManyToOne(targetEntity = Servicios.class)
	private Servicios servicio;
	
	@ManyToOne(targetEntity = Reserva.class)
	private Reserva reserva;
	
	

	
	public ReservaServicio() {
		
	}
	
	
	
	public ReservaServicio(int id, Date fechaReservada, Date fechaDevuelta, Servicios servicio, Reserva reserva) {
		this.id = id;
		this.fechaReservada = fechaReservada;
		this.fechaDevuelta = fechaDevuelta;
		this.servicio = servicio;
		this.reserva = reserva;
	}



	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Servicios getServicio() {
		return servicio;
	}
	public void setServicio(Servicios servicio) {
		this.servicio = servicio;
	}
	public Reserva getReserva() {
		return reserva;
	}
	public void setReserva(Reserva reserva) {
		this.reserva = reserva;
	}



	public Date getFechaReservada() {
		return fechaReservada;
	}



	public void setFechaReservada(Date fechaReservada) {
		this.fechaReservada = fechaReservada;
	}



	public Date getFechaDevuelta() {
		return fechaDevuelta;
	}



	public void setFechaDevuelta(Date fechaDevuelta) {
		this.fechaDevuelta = fechaDevuelta;
	}

	
	
	
}
