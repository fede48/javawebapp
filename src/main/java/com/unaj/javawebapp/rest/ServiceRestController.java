package com.unaj.javawebapp.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.unaj.javawebapp.model.Producto;
import com.unaj.javawebapp.model.Servicios;
import com.unaj.javawebapp.services.ProductoServiceImpl;
import com.unaj.javawebapp.services.ServiciosServiceImpl;

@RestController
public class ServiceRestController {
	
	private ServiciosServiceImpl serviceImpService;
	
	@Autowired
	public ServiceRestController(ServiciosServiceImpl servService) {
		this.serviceImpService=servService;
		// TODO Auto-generated constructor stub
	}
	
	@PostMapping("/servicio")
	public Servicios addservice(@RequestBody @Valid Servicios serv) {
		
		return serviceImpService.addServicio(serv);
	}
	
	@GetMapping("/servicio/{idOrg}")
	public List<Servicios>findAllByOrg(@PathVariable("idOrg") int id){
		
		return serviceImpService.listByOrganizacion(id);
		
	}
	
	

}