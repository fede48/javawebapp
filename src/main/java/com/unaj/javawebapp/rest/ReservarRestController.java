package com.unaj.javawebapp.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.unaj.javawebapp.model.EstadoReserva;
import com.unaj.javawebapp.model.Reserva;
import com.unaj.javawebapp.model.ReservaProducto;
import com.unaj.javawebapp.model.ReservaServicio;
import com.unaj.javawebapp.services.ReservaServiceImpl;

@RestController
public class ReservarRestController {
	
	private ReservaServiceImpl reservarserviceImp;
	
	@Autowired
	ReservarRestController(ReservaServiceImpl reser){
		this.reservarserviceImp=reser;
	}
	
	
	@PostMapping("/reservaproducto/{idUsr}")
	public Reserva newReserva(@RequestBody @Valid ReservaProducto reser,@PathVariable("idUsr")int id) {
		
		return reservarserviceImp.newReserva(reser,id);
	}
	
	@PostMapping("/reservaservicio/{idUsr}")
	public Reserva newReserva(@RequestBody @Valid ReservaServicio reser,@PathVariable("idUsr")int id) {
		
		return reservarserviceImp.newReserva(reser,id);
	}
	
	@GetMapping("/reserva")
	public List<Reserva>getReservas() {
		
		return reservarserviceImp.getReservas();
	}
	
	
	@DeleteMapping("/reserva")
	public Reserva deleteReservaPro(@RequestBody ObjectNode obj) {
		int idReserva=obj.get("reserva").asInt();
		int idReservapro=obj.get("reservapro").asInt();
		return reservarserviceImp.deleteReservaProducto(idReserva,idReservapro);
	}
	
	
	
	@PostMapping("/estadoReserva")
	public EstadoReserva newEstado(@RequestBody @Valid EstadoReserva estado ) {
		
		return reservarserviceImp.newEstadoReserva(estado);
	}
	
	

	
	
	

	
}
