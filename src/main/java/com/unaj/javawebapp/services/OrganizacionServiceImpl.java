package com.unaj.javawebapp.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.unaj.javawebapp.model.Organizacion;
import com.unaj.javawebapp.model.Usuario;
import com.unaj.javawebapp.repository.OrganizacionRepository;

@Service("servicioOrg")
public class OrganizacionServiceImpl{
	
	private OrganizacionRepository orgrepository;
	

	@Autowired
	public OrganizacionServiceImpl(OrganizacionRepository repoorg) {
		this.orgrepository=repoorg;
	}
	
	public Organizacion findById(int id) {
		
		return orgrepository.findById(id);
	}
	
	public boolean crearOrg(Organizacion organizacion) {
		try {
			orgrepository.save(organizacion);
			return true;
			
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	
	}
	
	public boolean actualizar(Organizacion organizacion) {
		try {
			if(orgrepository.existsById(organizacion.getId())) {
				orgrepository.saveAndFlush(organizacion);
			}
			else {
				this.crearOrg(organizacion);
			}
			
			
			return true;
			
			
		} catch (Exception e) {
			return false;
			// TODO: handle exception
		}
	}
	
	public boolean borrar(int id) {
		try {
			orgrepository.deleteById(id);
			return true;
			
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	
	}
	
	//probando manytomany
	

		
	
	
	
	public List<Organizacion> mostrartodo(){
		return orgrepository.findAll();
	}
	

		
	
	
	

}
