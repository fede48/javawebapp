package com.unaj.javawebapp.services;

import com.unaj.javawebapp.model.Organizacion;
import com.unaj.javawebapp.model.Rol;
import com.unaj.javawebapp.model.Usuario;
import com.unaj.javawebapp.repository.RolRepository;
import com.unaj.javawebapp.repository.UsuarioRepository;
import antlr.collections.*;
import java.util.*;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * user service implement.
 */
@Service("servicioUser")
public class UserServiceImpl  {

    private UsuarioRepository usuarioRepository;
    
    private RolRepository rolrepo;
    
    @Autowired
    public UserServiceImpl(UsuarioRepository usuariorepo,RolRepository rol) {
    	
    	this.usuarioRepository=usuariorepo;
    	this.rolrepo=rol;
    }

    public void setProductRepository(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    
    
	public Usuario crearUser(Usuario user) {			
		return usuarioRepository.saveAndFlush(user);

	
	}
	
	public String actualizar(Usuario user) {
		String msg = "";
		try {
			if(usuarioRepository.existsById(user.getId())) {
				usuarioRepository.saveAndFlush(user);
			}
			else {
				this.crearUser(user);
			}
			
			return "Se actualizo el usuario correctamente";
			
			
		} catch (Exception e) {
			return e.getMessage();
			// TODO: handle exception
		}
	}
	
	public String borrar(int id) {
		String msg = "";
		try {
			usuarioRepository.deleteById(id);
			return "Se Borro el usuario Existosamente";
			
		} catch (Exception e) {
			// TODO: handle exception
			return "Error Intente Nuevamente";
		}
	
	}
	
	public List<Usuario> mostrartodo(){
		return usuarioRepository.findAll();
	}
	
	public Usuario findByUsername(String username) {
		
		return usuarioRepository.findByUserName(username);
	}

	public Rol nuevoRol(Rol rol) {
		// TODO Auto-generated method stub
		return rolrepo.save(rol);
	}
	
	public List<Rol> listRoles() {
		// TODO Auto-generated method stub
		return rolrepo.findAll();
	}
	

	
    
   

}
