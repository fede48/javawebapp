package com.unaj.javawebapp.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.unaj.javawebapp.model.Producto;
import com.unaj.javawebapp.repository.ProductoRepository;

@Service("serviceProducto")
public class ProductoServiceImpl {
	
	
	private ProductoRepository repositoryProduct;
	
	@Autowired
	public ProductoServiceImpl(ProductoRepository productrepo) {
		this.repositoryProduct=productrepo;
	}
	
	
	
	public Producto addProduct(Producto product) {
		
		return repositoryProduct.save(product);
		
	}
	
	public List<Producto> listByOrganizacion(int id){
		
		return repositoryProduct.findByOrganizacion(id);
	}
	
	
	
	public String deletebyId(Integer id) {
		
		repositoryProduct.deleteById(id);
		
		return "Producto "+id+" borrado";
		}



	public List<Producto> listAll() {
		// TODO Auto-generated method stub
		return repositoryProduct.findAll();
	}
	
	
	

}
