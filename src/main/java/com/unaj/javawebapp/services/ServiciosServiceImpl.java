package com.unaj.javawebapp.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.unaj.javawebapp.model.Producto;
import com.unaj.javawebapp.model.Servicios;
import com.unaj.javawebapp.repository.ServiciosRepository;

@Service("serviceServicios")
public class ServiciosServiceImpl {
	
	
	private ServiciosRepository repositoryServicios;
	
	@Autowired
	ServiciosServiceImpl(ServiciosRepository servicesRepository){
		this.repositoryServicios = servicesRepository;
	}
	
	public Servicios addServicio(Servicios service) {
		
		return repositoryServicios.save(service);
		
	}
	
	public List<Servicios> listByOrganizacion(int id){
		
		return repositoryServicios.findByOrganizacion(id);
	}
	
	
	
	public String deletebyId(Integer id) {
		
		repositoryServicios.deleteById(id);
		
		return "Producto "+id+" borrado";
		}
	
	
	

	
	
	

}
