package com.unaj.javawebapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.unaj.javawebapp.model.Usuario;
import com.unaj.javawebapp.services.UserServiceImpl;

@Controller
public class LoginController {
	
	private UserServiceImpl userService;
	
	@Autowired
	public LoginController(UserServiceImpl userser) {
		this.userService=userser;
	}
	
	@PostMapping("/login")
	public String ShowReservaspage(@ModelAttribute(name = "loginForm") Usuario loginForm, Model model) {
				
		String username=loginForm.getUsername();
		String password=loginForm.getPassword();
		
		Usuario user=userService.findByUsername(username);

		if(username.equals(user.getUsername()) && password.equals(user.getPassword())) {
			
			return "/prueba";
		}
		else {
			return "/login";
		}
		
		 
		
		
	}

	@GetMapping("/register")
	public String goToresgister() {
		return "views/createUser";
	}
}
