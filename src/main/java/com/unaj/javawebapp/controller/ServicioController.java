package com.unaj.javawebapp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.unaj.javawebapp.model.Organizacion;
import com.unaj.javawebapp.model.Producto;
import com.unaj.javawebapp.model.Servicios;
import com.unaj.javawebapp.services.ServiciosServiceImpl;
import com.unaj.javawebapp.services.OrganizacionServiceImpl;


@Controller
public class ServicioController {
	
	private ServiciosServiceImpl serviceService;
	private OrganizacionServiceImpl serviceOrganizacion;
	
	@Autowired
	public ServicioController(ServiciosServiceImpl serviceService, OrganizacionServiceImpl serviceOrganizacion) {
		this.serviceService=serviceService;
		this.serviceOrganizacion = serviceOrganizacion;
		// TODO Auto-generated constructor stub
	}
	
	
	@GetMapping("/cformService")
    public String showFormService(Servicios service) {
        return "views/createService";
    }
	
	@PostMapping("/cservicio")
	public String addservice(@Valid Servicios servicio, BindingResult result, Model model) {
		
		//if (result.hasErrors()) {
        //    return "views/createService";
       // }
         
		Organizacion orga= serviceOrganizacion.findById(0);
		servicio.setOrganizacion(orga);
		serviceService.addServicio(servicio);
        
		List<Servicios> list = serviceService.listByOrganizacion(0);
		model.addAttribute("services", list);
        return "views/listServices";

	}
	
	@GetMapping("/cservicio/{idOrg}")
	public String findAllByOrg(@PathVariable("idOrg") int id, ModelMap map){
		
		List<Servicios> list = serviceService.listByOrganizacion(id);
		map.addAttribute("services", list);
        return "views/listServices";
		
		//return "views/listProductos";  
	}
	
	

}
