package com.unaj.javawebapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	@GetMapping(path = {"/","/index"})
	public String index() {
		//Usuario user = new Usuario();
		//user.setUsername("fede");
		//user.setApellido("acunia");
		//user.setEmail("fede.est4@gmail.com");
		//user.setPassword(encoder.encode("147"));
		//userServiceImp.saveUsuario(user);
		return "index";
	}
	 
	@GetMapping("/login") 
	public String showLoginForm() {
			
		return "views/loginForm";  
	}
	
	
	@GetMapping("/listarorganizacion")
	public String showOrganizacionList() {
		return "views/listOrganizacion";
	}
	

	@GetMapping("/formCrearOrganizacion")
	public String showCrearOrganizacion() {
		return "views/createOrganizacion";
	}
	
  
}