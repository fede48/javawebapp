package com.unaj.javawebapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.unaj.javawebapp.model.ReservaProducto;
import com.unaj.javawebapp.model.Servicios;
import com.unaj.javawebapp.services.ReservaServiceImpl;

@Controller
public class ReservasProductController {
	
	private ReservaServiceImpl reservarserviceImp;
	
	@Autowired
	public ReservasProductController(ReservaServiceImpl reservarservice) {
		this.reservarserviceImp=reservarservice;
		// TODO Auto-generated constructor stub
	}

	
	@GetMapping("/reservarProducto")
	public String findAllByOrg(){
		
		
        return "views/listReservas";
		
		//return "views/listProductos";  
			
	}
	@GetMapping("/formReservarProducto")
		public String formReservas(){
			return "views/createReserva";
		}
		
	

}
