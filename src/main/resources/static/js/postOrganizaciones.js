$( document ).ready(function() {

  // SUBMIT FORM
    $("#organizacionForm").submit(function(event) {
    // Prevent the form from submitting via the browser.
    event.preventDefault();
    ajaxPost();
    
  });
     
    function ajaxPost(){
      
      // PREPARE FORM DATA
      var formData = { "descripcion": $("#descripcion").val(), "email": $("#email").val(),
    		  		    "nombre": $("#nombre").val()	
                     }
     
      
      
      // DO POST
      $.ajax({
      type : "POST",
      contentType : "application/json",
      dataType : 'json',
      url : "http://localhost:8080/organizacion",
      data : JSON.stringify(formData),
      success :function(result) {
    	  alert("guardado exitoso");
    	  document.location.href = "http://localhost:8080/listarorganizacion"

      },
    	    error : function(e) {
    	        alert("Error!");
    	        console.log("ERROR: ", e);
    	      }
    });
      
     
 
    }
    
})